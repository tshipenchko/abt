# Aiogram Bot Template
My Default template for telegram bots written with aiogram

# How to run
```commandline
pip3 install -r requerments.txt
python3 -m app -c config.toml
```

# Configure storage
db.storage is memory.MemoryStorage by default. <br/>
db.storage_args is empty dict by default.

# Configure i18n
```commandline
pybabel extract -k gt --input-dirs=app -o locales/bot.pot 
pybabel init -i locales/bot.pot -d locales -D bot -l langCode
nano locales/en/LC_MESSAGES/bot.po
pybabel compile -d locales -D bot
```
When you change the code of bot you need to update po & mo files.
```commandline
pybabel extract -k gt --input-dirs=. -o locales/bot.pot 
pybabel update -d locales -D bot -i locales/bot.pot
nano locales/en/LC_MESSAGES/bot.po
pybabel compile -d locales -D mybot
```

[comment]: <> (## Use webhook)

[comment]: <> (Add [webhook] to config file, and set up fields:)

[comment]: <> ( * host)

[comment]: <> ( * port)

[comment]: <> ( * url_path)

[comment]: <> ( * protocol)

[comment]: <> ( * certificate)

[comment]: <> ( * private_key)

## Local bot-API
Add api field to [bot] with local bot-API, default is https://api.telegram.org
