def register(dp, config, session_maker, gettext):
    from . import throttling
    from . import f
    throttling.register(dp, config, gettext)
    f.register(dp, config, session_maker, gettext)

