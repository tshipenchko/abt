import inspect

from aiogram import types
from aiogram.dispatcher.handler import current_handler
from aiogram.dispatcher.middlewares import BaseMiddleware

from app.common import FMT


class FMTMiddleware(BaseMiddleware):
    def __init__(self, config, session_maker, gettext):
        self.config = config
        self.session_maker = session_maker
        self.gettext = gettext
        super().__init__()

    async def on_process_message(self, _: types.Message, data: dict):
        return await self.open(data)

    async def on_process_callback_query(self, _: types.CallbackQuery, data: dict):
        return await self.open(data)

    async def on_post_process_message(self, _: types.Message, __: list, data: dict):
        return await self.close(data)

    async def on_post_process_callback_query(self, _: types.CallbackQuery, __: list, data: dict):
        return await self.close(data)

    async def open(self, data: dict):
        handler = current_handler.get()
        args = inspect.getfullargspec(handler)[0]

        data["gt"] = self.gettext

        async with self.session_maker() as session:
            if "f" in args:
                data["f"] = FMT(
                    db=session,
                    config=self.config
                )

        return True

    @staticmethod
    async def close(data: dict):
        if "f" in data:
            f: FMT = data["f"]
            await f.db.close()

        return True


def register(dp, config, session_maker, gettext):
    dp.middleware.setup(FMTMiddleware(config, session_maker, gettext))
