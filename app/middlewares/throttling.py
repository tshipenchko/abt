from aiogram import Dispatcher, types
from aiogram.dispatcher.handler import CancelHandler
from aiogram.dispatcher.middlewares import BaseMiddleware
from aiogram.utils import exceptions
from aiogram.utils.exceptions import Throttled

from app.config_parser import Config


class ThrottlingMiddleware(BaseMiddleware):
    def __init__(self, config: Config, gettext, limit=2, key="antiflood__message"):
        self.config = config
        self.limit = limit
        self.key = key
        self.gt = gettext
        super(ThrottlingMiddleware, self).__init__()

    async def on_process_message(self, message: types.Message, _: dict):
        if message.from_user.id in self.config.bot.admins:
            return

        dispatcher = Dispatcher.get_current()
        try:
            await dispatcher.throttle(self.key, rate=self.limit)
        except Throttled as t:
            if t.exceeded_count <= 2:
                try:
                    await message.answer(self.gt("Stop flood! Otherwise I will stop responding..."))
                except exceptions.BadRequest:
                    pass

            raise CancelHandler() from t


def register(dp, config, gettext):
    dp.middleware.setup(ThrottlingMiddleware(config, gettext))
