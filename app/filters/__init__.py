def register(dp, config):
    from . import is_admin
    is_admin.register(dp, config)
