from aiogram import types
from aiogram.dispatcher.filters import BoundFilter

from typing import Union

from app.config_parser import Config


def generate(config: Config):
    class IsAdmin(BoundFilter):
        key = "is_admin"

        def __init__(self, is_admin):
            self.is_admin = is_admin

        async def check(self, event: Union[types.Message, types.CallbackQuery]) -> bool:
            return event.from_user.id in config.bot.admins

    return IsAdmin


def register(dp, config):
    dp.filters_factory.bind(generate(config),
                            event_handlers=[dp.message_handlers, dp.callback_query_handlers])
