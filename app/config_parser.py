import os
from dataclasses import dataclass, fields, MISSING, field
from typing import List

import toml


@dataclass
class ConfigBot:
    token: str
    admins: List[int]

    api: str = "https://api.telegram.org/"


@dataclass
class ConfigDB:
    engine: str
    storage: str = "memory.MemoryStorage"
    storage_args: dict = field(default_factory=dict)


@dataclass
class Config:
    bot: ConfigBot
    db: ConfigDB

    @classmethod
    def parse(cls, data: dict) -> "Config":
        sections = {}

        for section in fields(cls):
            pre = {}
            current = data[section.name]

            for field_ in fields(section.type):
                if field_.name in current:
                    pre[field_.name] = current[field_.name]
                elif field_.default != MISSING:
                    pre[field_.name] = field_.default
                elif field_.default_factory != MISSING:
                    pre[field_.name] = field_.default_factory()
                else:
                    raise ValueError(f"Missing field {field_.name} in section {section.name}")

            sections[section.name] = section.type(**pre)

        return cls(**sections)


def parse_config(config_file: str) -> Config:
    if not os.path.isfile(config_file) and not config_file.endswith(".toml"):
        config_file += ".toml"

    if not os.path.isfile(config_file):
        raise FileNotFoundError(f"Config file not found: {config_file} no such file")

    with open(config_file, "r") as f:
        data = toml.load(f)

    return Config.parse(dict(data))
