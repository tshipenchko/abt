"""Application running"""
import asyncio
import logging
import argparse
import importlib
from pathlib import Path

from aiogram import Bot, Dispatcher
from aiogram.bot.api import TelegramAPIServer
from aiogram.contrib.middlewares.i18n import I18nMiddleware
from aiogram.types import BotCommand
from aiogram.types.bot_command_scope import BotCommandScopeDefault

from app import db
from app.config_parser import parse_config
from app.utils import get_handled_updates_list


def parse_arguments():
    parser = argparse.ArgumentParser(description="Process bot configuration.")
    parser.add_argument("--config", "-c", type=str, help="configuration file", default="config.toml")
    parser.add_argument("--locales", "-l", type=str, help="locales directory",
                        default=Path(__file__).parent.parent / "locales")
    parser.add_argument("--set-commands", "-s", action="store_true")

    return parser.parse_args()


async def set_bot_commands(bot: Bot):
    commands = [
        BotCommand(command="start", description="Just start")
    ]
    await bot.set_my_commands(commands, scope=BotCommandScopeDefault())


def register_all(dp, config, session_maker, gettext):
    from app import filters
    filters.register(dp, config)

    from app import handlers
    handlers.register(dp)

    from app import middlewares
    middlewares.register(dp, config, session_maker, gettext)


def import_(name: str):
    components = name.rsplit(".", maxsplit=1)
    return getattr(importlib.import_module(components[0]), components[-1])


async def main():
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s|%(levelname)s|%(name)s|%(message)s",
        datefmt='%Y-%m-%d|%H:%M:%S',
    )

    arguments = parse_arguments()
    config = parse_config(arguments.config)

    async_sessionmanager = await db.init(config.db.engine)

    api_server = TelegramAPIServer.from_base(config.bot.api)
    bot = Bot(config.bot.token, parse_mode="HTML", server=api_server)
    storage = import_(f"aiogram.contrib.fsm_storage.{config.db.storage}")(**config.db.storage_args)
    dp = Dispatcher(bot, storage=storage)

    i18n = I18nMiddleware("bot", arguments.locales)
    dp.setup_middleware(i18n)
    gettext = i18n.gettext

    register_all(dp, config, async_sessionmanager, gettext)

    if arguments.set_commands:
        await set_bot_commands(bot)

    try:  # TODO: add weebhooks
        await dp.start_polling(allowed_updates=get_handled_updates_list(dp))
    finally:
        await dp.storage.close()
        await dp.storage.wait_closed()
        session = await bot.get_session()
        if session:
            await session.close()

try:
    asyncio.run(main())
except (KeyboardInterrupt, SystemExit):
    logging.error("Bot stopped!")
