from aiogram import types

from app.common import FMT


async def cmd_start(message: types.Message, f: FMT, gt: callable):
    if not await f.db.is_registered(message.from_user.id):
        await f.db.register(message.from_user.id)

    await message.reply(gt("Start message"))


def register(dp):
    dp.register_message_handler(cmd_start, commands="start")
