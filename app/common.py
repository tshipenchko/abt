"""There is some needed objects"""
from dataclasses import dataclass
from typing import Optional

from app.config_parser import Config
from app.db.functions import DB


@dataclass
class FMT:
    db: Optional[DB]
    config: Config
